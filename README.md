# Dot files
These are highly inspired by Derek Taylor over on DistroTube on YouTube
Original dot files come from him, I have just modified them and added to them


## What is a dot file
A dot file is the files in your home directory in GNU/Linux that often start with a period. For example some would be ~/.bashrc if you were using bash.


## Needed stuff
Original credit for most of these come from [https://gitlab.com/dwt1] @ DistroTube
